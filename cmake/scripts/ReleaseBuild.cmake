#
# This is a ctest script to build a project from scratch. It assumes
# that is is called from build_release with various environment variables
# properly set.
#
# Required:
#
# CHECKOUT_AREA - base of checked out code
# BUILD_AREA    - base of build area
# PROJECT       - project name
# BRANCH        - branch
# CONFIG        - build configuration
#
# Optional:
#
# CHECKOUT_OPTIONS - options to checkout_release
# CMAKE_OPTIONS    - options to cmake configuration step
#
# ctest variables (all optional), set with -D VAR=VALUE
#
# CTEST_BUILD_NAME - customize build name for CDash
# CTEST_MODE       - Should be mostly Experimental
# CTEST_TRACK      - Nightly, Continuous, Experimental, dev3, dev4
# CTEST_CORES      - Number of cores to use when building
# CTEST_NOCLEAN    - Do not start from clean build dir.
# CTEST_NOTESTS    - Do not execute tests.
# CTEST_NO_SUBMIT  - Do not submit to CDash.
#
set(CTEST_SOURCE_DIRECTORY "$ENV{CHECKOUT_AREA}/$ENV{PROJECT}/$ENV{BRANCH}")
set(CTEST_BINARY_DIRECTORY "$ENV{BUILD_AREA}/$ENV{PROJECT}/$ENV{BRANCH}/$ENV{CONFIG}")

if(NOT DEFINED CTEST_BUILD_NAME)
  set(CTEST_BUILD_NAME "$ENV{BRANCH}-$ENV{CONFIG}")
endif()

set(CTEST_NIGHTLY_START_TIME "00:00:00 CEST")

if(NOT DEFINED CTEST_MODE)
  set(CTEST_MODE Experimental)
endif()

if(NOT DEFINED CTEST_NOCLEAN)
  ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
endif()

if(DEFINED CTEST_TRACK)
  set(CTEST_TRACK TRACK ${CTEST_TRACK})
endif()

set(CTEST_UPDATE_VERSION_ONLY TRUE)
set(CTEST_UPDATE_COMMAND git)

find_program(hostname NAMES hostname)
execute_process(COMMAND ${hostname} OUTPUT_VARIABLE CTEST_SITE OUTPUT_STRIP_TRAILING_WHITESPACE)

find_program(checkout_release NAMES checkout_release)
set(CTEST_CHECKOUT_COMMAND "${checkout_release} $ENV{CHECKOUT_OPTIONS} $ENV{PROJECT} $ENV{BRANCH}")

find_program(cmake_config NAMES cmake_config)
set(CTEST_CONFIGURE_COMMAND "${cmake_config} $ENV{CONFIG} ${CTEST_SOURCE_DIRECTORY} -- $ENV{CMAKE_OPTIONS}")

if(DEFINED CTEST_CORES)
  set(NumCores ${CTEST_CORES})
else()
  include(ProcessorCount)
  ProcessorCount(NumCores)
endif()

if(${NumCores} GREATER 0)
set(CTEST_BUILD_FLAGS "-j ${NumCores}")
endif()

find_program(make NAMES make)
set(CTEST_BUILD_COMMAND "${make} ${CTEST_BUILD_FLAGS} -k all install/fast")

ctest_start(${CTEST_MODE} ${CTEST_TRACK})
if(NOT DEFINED CTEST_NO_SUBMIT)
  ctest_submit(QUIET PARTS Start RETURN_VALUE submit_result CAPTURE_CMAKE_ERROR submit_error)
endif()

ctest_update(SOURCE ${CTEST_SOURCE_DIRECTORY})
if(NOT DEFINED CTEST_NO_SUBMIT)
  ctest_submit(QUIET PARTS Update RETURN_VALUE submit_result CAPTURE_CMAKE_ERROR submit_error)
endif()

ctest_read_custom_files(${CTEST_SOURCE_DIRECTORY}/cmake)
ctest_configure(QUIET)
if(NOT DEFINED CTEST_NO_SUBMIT)
  ctest_submit(QUIET PARTS Configure RETURN_VALUE submit_result CAPTURE_CMAKE_ERROR submit_error)
endif()

ctest_read_custom_files(${CTEST_BINARY_DIRECTORY}/cmake)
ctest_build(QUIET NUMBER_ERRORS num_errors)
if(NOT DEFINED CTEST_NO_SUBMIT)
  ctest_submit(QUIET PARTS Build RETURN_VALUE submit_result CAPTURE_CMAKE_ERROR submit_error)
endif()

if(NOT DEFINED CTEST_NOTESTS)
  ctest_test(QUIET)
  if(NOT DEFINED CTEST_NO_SUBMIT)
    ctest_submit(QUIET PARTS Test RETURN_VALUE submit_result CAPTURE_CMAKE_ERROR submit_error)
  endif()
endif()

if(NOT DEFINED CTEST_NO_SUBMIT)
  ctest_submit(QUIET PARTS Done RETURN_VALUE submit_result CAPTURE_CMAKE_ERROR submit_error)
endif()

if(NOT ${num_errors} EQUAL 0)
  # Generate a fatal error if there was a build failure
  message(FATAL_ERROR "Build errors: ${num_errors}")
endif()
