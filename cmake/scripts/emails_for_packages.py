#!/usr/bin/env python
#
# You should set the GITTOKEN environment variable
# to an access token for gitlab.cern.ch
#
# This script retrieves the full list of packages
# for a given project and builds a table of
# package_name followed by a comma separated list of
# e-mail addresses of developers.
#
import sys
import os
import requests
import json
import string
import ldap
from pprint import pprint

con = con = ldap.initialize('ldap://xldap.cern.ch') 
con.simple_bind_s()
base_dn = 'ou=Users,ou=Organic Units,dc=cern,dc=ch'
attrs = ['mail']

cache = {}

def get_email(user):
    if cache.has_key(user):
        return cache[user]
    try:
        email = con.search_s( base_dn, ldap.SCOPE_SUBTREE, '(cn=%s)' % user, attrs)[0][1]['mail'][0]
        cache[user] = email
        return email
    except:
        # should never happen ;)
        return 'unknown@cern.ch'
    
url = 'https://gitlab.cern.ch/api/v4/'
token = os.environ['GITTOKEN']
project = os.environ.get('GITPROJECT','atlas-tdaq-software')

headers = { 'PRIVATE-TOKEN': token}

if len(sys.argv) > 1:
    projects = [ requests.get(url + 'projects/%s%%2F%s' % (project, p), headers=headers).json() for p in sys.argv[1:] ]
else:
    projects =  requests.get(url + 'groups/%s' % project, headers=headers).json()['projects']

for prj in projects:
    print prj['name'],
    members = requests.get(url + 'projects/%d/members' % prj['id'], headers=headers).json()   
    print string.join([ get_email(m['username']) for m in members if m['access_level'] >= 30 and m['username'] != 'atdsoft'],',' )
