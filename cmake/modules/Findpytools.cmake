# - Simple module to add the pytools directory to the python path
#
# PYTOOLS_FOUND
# PYTOOLS_PYTHON_PATH
# PYTOOLS_BINARY_PATH

set(PYTOOLS_FOUND 1)
file(GLOB  PYTOOLS_PYTHON_PATH ${PYTOOLS_ROOT}/lib/python*/site-packages)
set(PYTOOLS_PYTHON_PATH ${PYTOOLS_PYTHON_PATH}
    CACHE PATH "Path to the pytools LCG package (Python modules)")

set(PYTOOLS_BINARY_PATH ${PYTOOLS_ROOT}/bin
    CACHE PATH "Path to the pytools LCG package (scripts)")

mark_as_advanced(PYTOOLS_FOUND PYTOOLS_PYTHON_PATH PYTOOLS_BINARY_PATH)
