#
# FELIX Specific external library versions
#

#
# Specific versions for LCG libraries are specified in cmake_tdaq/bin/setup.sh
#
set(qt5_version                      "$ENV{QT5_VERSION}")
set(sqlite_version                   "$ENV{FELIX_SQLITE_VERSION}")
set(tbb_version                      "$ENV{FELIX_TBB_VERSION}")
set(boost_version                    "$ENV{FELIX_BOOST_VERSION}")
set(pythonlibs_version               "$ENV{FELIX_PYTHONLIBS_VERSION}")
set(zeromq_version                   "$ENV{FELIX_ZEROMQ_VERSION}")
set(libsodium_version                "$ENV{FELIX_LIBSODIUM_VERSION}")

#
# FELIX Package versions are specified in cmake_tdaq/bin/setup.sh
#
set(bootstrap_version               "$ENV{FELIX_BOOSTRAP_VERSION}")
set(catch_version                   "$ENV{FELIX_CATCH_VERSION}")
set(concurrentqueue_version         "$ENV{FELIX_CONCURRENTQUEUE_VERSION}")
set(czmq_version                    "$ENV{FELIX_CZMQ_VERSION}")
set(datatables_version              "$ENV{FELIX_DATATABLES_VERSION}")
set(docopt_version                  "$ENV{FELIX_DOCOPT_VERSION}")
set(felix_drivers_version           "$ENV{FELIX_DRIVERS_VERSION}")
set(fontawesome_version             "$ENV{FELIX_FONTAWESOME_VERSION}")
set(highcharts_version              "$ENV{FELIX_HIGHCHARTS_VERSION}")
set(jquery_version                  "$ENV{FELIX_JQUERY_VERSION}")
set(json_version                    "$ENV{FELIX_JSON_VERSION}")
set(jwrite_version                  "$ENV{FELIX_JWRITE_VERSION}")
set(libcurl_version                 "$ENV{FELIX_LIBCURL_VERSION}")
set(libfabric_version               "$ENV{FELIX_LIBFABRIC_VERSION}")
set(libnuma_version                 "$ENV{FELIX_LIBNUMA_VERSION}")
set(logrxi_version                  "$ENV{FELIX_LOGRXI_VERSION}")
set(mathjs_version                  "$ENV{FELIX_MATHJS_VERSION}")
set(moment_version                  "$ENV{FELIX_MOMENT_VERSION}")
set(patchelf_version                "$ENV{FELIX_PATCHELF_VERSION}")
set(prometheus_cpp_version          "$ENV{FELIX_PROMETHEUS_CPP_VERSION}")
set(pybind11_version                "$ENV{FELIX_PYBIND11_VERSION}")
set(readerwriterqueue_version       "$ENV{FELIX_READERWRITERQUEUE_VERSION}")
set(simdjson_version                "$ENV{FELIX_SIMDJSON_VERSION}")
set(simplewebserver_version         "$ENV{FELIX_SIMPLEWEBSERVER_VERSION}")
set(spdlog_version                  "$ENV{FELIX_SPDLOG_VERSION}")
set(yaml-cpp_version                "$ENV{FELIX_YAMPCPP_VERSION}")
set(zyre_version                    "$ENV{FELIX_ZYRE_VERSION}")
