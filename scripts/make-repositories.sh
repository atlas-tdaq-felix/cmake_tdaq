#!/bin/bash
#
# This script reproduces what happens at install time to collect all
# the information and create the OKS software repositories and externals
# for tdaq-common and tdaq. The output files will be in local directory
# `pwd`/daq/sw/*.data.xml.
#
# Arguments: none
#

[ -n "${TDAQ_INST_PATH}" ] || { echo "You must set up a TDAQ release before running this script"; exit 1; }

mkdir -p daq/sw || { echo "Cannot create output directory"; exit 1; }
export TDAQ_DB_PATH=`pwd`:$TDAQ_DB_PATH

create_tags.py x86_64-{slc6,centos7}-{gcc62,gcc8}-{opt,dbg} && mv tags.data.xml daq/sw || exit 2
create_externals.py 'TDAQ Common' `pwd`/daq/sw/tdaq-common-external-repository.data.xml ${TDAQC_INST_PATH}/share/cmake/sw_repo.yaml daq/sw/tags.data.xml || exit 3
create_repo.py 'TDAQ Common' `pwd`/daq/sw/tdaq-common-repository.data.xml ${TDAQC_INST_PATH}/share/cmake/sw_repo.yaml daq/sw/tags.data.xml daq/sw/tags.data.xml daq/sw/tdaq-common-external-repository.data.xml || exit 4
create_externals.py 'Online' `pwd`/daq/sw/external.data.xml ${TDAQ_INST_PATH}/share/cmake/sw_repo.yaml daq/sw/tags.data.xml || exit 5 
create_repo.py 'Online' `pwd`/daq/sw/repository.data.xml ${TDAQ_INST_PATH}/share/cmake/sw_repo.yaml daq/sw/tags.data.xml daq/sw/tdaq-common-repository.data.xml daq/segments/common-environment.data.xml daq/sw/external.data.xml daq/sw/common-resources.data.xml || exit 6
