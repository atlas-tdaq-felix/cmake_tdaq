#!/bin/bash
#
# Install normal and dev nightlies in the current directory.
# The versions and configurations to install can be controlled by
# the VERSIONS= and CONFIGS= environment variables.
#
# The installation directory can be passed as the first parameter.
#
export VERSIONS=${VERSIONS:=tdaq-99-00-00 tdaq-99-00-01}
export CONFIGS=${CONFIGS:=x86_64-centos7-gcc8-opt}
export SERVER=${SERVER:=https://test-atdaq-sw.web.cern.ch/test-atdaq-sw/nightlies}
export ECHO=${ECHO:=echo}
export INSTALL_DIR=${1:-$(pwd)}

echo "Installation directory: ${INSTALL_DIR}"
cd ${INSTALL_DIR} || { exit 1; }

for version in ${VERSIONS}
do
    version=${version#tdaq-}

    # once per version
    ${ECHO} "Preparing installation for tdaq-${version}"
    mkdir -p tdaq/tdaq-${version} tdaq-common/tdaq-common-${version}
    rm -rf tdaq/tdaq-${version}/installed tdaq-common/tdaq-common-${version}/installed
    
    # once per configuration per version
    for config in ${CONFIGS}
    do
        ${ECHO} "Installing tdaq-${version} ${config}"
        (cd tdaq-common/tdaq-common-${version} && curl -s ${SERVER}/tdaq-common-${version}-${config}.tar.gz | tar zxf -)
        (cd tdaq/tdaq-${version} && curl -s ${SERVER}/tdaq-${version}-${config}.tar.gz | tar zxf -)
    done
done
(cd tdaq &&  ln -snf tdaq-99-00-00 nightly && ln -snf tdaq-99-00-01 dev)
for dir in tdaq-common/tdaq-common-* tdaq/tdaq-*
do
    if [ -d ${dir}/.git ]; then
        echo "Updating source in ${dir}"
        (cd ${dir}; git pull)
    fi
done
